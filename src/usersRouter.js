const express = require('express');

const router = express.Router();
const { getProfile, deleteProfile, changePassword } = require('./usersService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

router.patch('/', authMiddleware, changePassword);

router.get('/', authMiddleware, getProfile);

router.delete('/', authMiddleware, deleteProfile);

module.exports = {
  usersRouter: router,
};