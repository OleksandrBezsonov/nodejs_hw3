const express = require('express');

const router = express.Router();
const {
    createTruck, getTrucks, getTruckById, updateTruck, assignTruck, deleteTruck, 
} = require('./truckService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

const asyncWrapper = (controller) => {
    return (req, res, next) => controller(req, res, next).catch(next);
}

router.post('/', authMiddleware, asyncWrapper(createTruck));

router.get('/', authMiddleware, asyncWrapper(getTrucks));

router.get('/:id', authMiddleware, asyncWrapper(getTruckById));

router.put('/:id', authMiddleware, asyncWrapper(updateTruck));

router.post('/:id/assign', authMiddleware, asyncWrapper(assignTruck));

router.delete('/:id', authMiddleware, asyncWrapper(deleteTruck));

module.exports = {
    truckRouter: router,
  };