const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://alex:bess@cluster1.tuuq1i0.mongodb.net/todoapp');

const { authRouter } = require('./authRouter.js');
const { usersRouter } = require('./usersRouter.js');
const { loadRouter } = require('./loadRouter.js');
const { truckRouter } = require('./truckRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(400).send({ message: err });
}

// function errorHandler(err, req, res, next) {
//   console.error(err);
//   res.status(500).send({ message: 'Server error' });
// }
