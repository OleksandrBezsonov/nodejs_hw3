const express = require('express');

const router = express.Router();
const {
    createLoad, createLoadById, getLoads, getLoadById, getActiveLoads, getLoadInfo, updateLoad, changeLoadState,
    deleteLoad,
} = require('./loadService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

const asyncWrapper = (controller) => {
    return (req, res, next) => controller(req, res, next).catch(next);
}

router.post('/', authMiddleware, asyncWrapper(createLoad));

router.post('/:id/post', authMiddleware, asyncWrapper(createLoadById));

router.get('/', authMiddleware, asyncWrapper(getLoads));

router.get('/', authMiddleware, asyncWrapper(getLoadById));

router.get('/active', authMiddleware, asyncWrapper(getActiveLoads));

router.get('/', authMiddleware, asyncWrapper(getLoadInfo));

router.patch('/active/state', authMiddleware, asyncWrapper(changeLoadState));

router.put('/:id', authMiddleware, asyncWrapper(updateLoad));

router.delete('/:id', authMiddleware, asyncWrapper(deleteLoad));

module.exports = {
  loadRouter: router,
};