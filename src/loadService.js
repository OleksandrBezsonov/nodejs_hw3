const { Load, loadJoiSchema } = require('./models/Loads.js');
const { Truck, truckJoiSchema } = require('./models/Trucks.js');

const compare = (obj1, obj2, propertyToCompare) => {
  return obj1[propertyToCompare] <= obj2[propertyToCompare];
}

const loadStates = [
  "En route to Pick Up",
  "Arrived to Pick Up",
  "En route to delivery",
  "Arrived to delivery",
]

const truckTypes = {
  "SPRINTER": {
    "dimensions": {
      "width": 300,
      "length": 250,
      "height": 170,
    },
    "payload": 1700,
  },
  "SMALL_STRAIT": {
    "dimensions": {
      "width": 500,
      "length": 250,
      "height": 170,
    },
    "payload": 2500,
  },
  "LARGE_STRAIT": {
    "dimensions": {
      "width": 700,
      "length": 350,
      "height": 270,
    },
    "payload": 3000,
  },
};

async function createLoad(req, res, next) {
  if (req.user.role === "SHIPPER") {
    const {
      name, payload, pickup_address, delivery_address,
      dimensions,
    } = req.body;
    console.log(req.body);
    const load = new Load({
      createdBy: req.user.userId,
      status: 'NEW',
      name: name,
      payload: payload,
      pickup_address: pickup_address,
      delivery_address: delivery_address,
      dimensions: dimensions,
      createdDate: new Date(),
    });
    load.save()
    .then(() => res.json({
    message: "Success"
    }))
  } else {
    err = 'Only for shippers';
    next(err);
  };
}

async function createLoadById(req, res, next) {
  if (req.user.role === "SHIPPER") {
    let load;
    let trucktype = [];
    await Load.findByIdAndUpdate({ _id: req.params.id}, { $set: { status: 'POSTED' } })
    .then((saved) => load = saved);
    if(load.payload < truckTypes.SPRINTER.payload && Object.keys(load.dimensions).every(prop => {
        return compare(load.dimensions, truckTypes.SPRINTER.dimensions, prop);
      })
    ){
      trucktype.push('SPRINTER', 'SMALL_STRAIT', 'LARGE_STRAIT')
    } else if (load.payload < truckTypes.SPRINTER.payload && Object.keys(load.dimensions).every(prop => {
          return compare(load.dimensions, truckTypes.SMALL_STRAIT.dimensions, prop);
        })
    ) {
      trucktype.push('SMALL_STRAIT', 'LARGE_STRAIT')
    } else if (load.payload < truckTypes.SPRINTER.payload && Object.keys(load.dimensions).every(prop => {
          return compare(load.dimensions, truckTypes.LARGE_STRAIT.dimensions, prop);
        })
    ) {
      trucktype.push('LARGE_STRAIT')
    } else next(err = "Load is too large");
    console.log(trucktype)
    let truck = await Truck.findOne({ status: 'IS', type: { $in: trucktype }, assignedTo: {$ne:null}});
    if(truck){
      truck.status = 'OL';
      truck.save().then((saved) => console.log(saved));
      console.log("status change");
      load.status = "ASSIGNED";
      load.assignedTo = truck.assignedTo;
      load.state = 'En route to Pick Up';
      load.save().then((saved) => console.log(saved));
      return res.send({
        message: "Load posted successfull",
        driver_found: true,
      });
    } else {
      return res.send({
        message: "Load posted successfull",
        driver_found: false,
      });
    }
  } else {
    err = 'Only for shippers';
    next(err);
  };
}

function getLoads(req, res, next) {
  if (req.user.role === "SHIPPER") {
    return Load.find({ userId: req.user.userId }, '-__v').then((result) => {
      res.json({loads: result});
    });
  } else {
    err = 'Only for shippers';
    next(err);
  };
}

function getLoadById(req, res, next) {
  if (req.user.role === "SHIPPER") {

  }
}

function getActiveLoads(req, res, next) {
  if (req.user.role === "DRIVER") {
    return Load.find({ assignedTo: req.user.userId }).then((result) => {
      res.json({load: result[0]});
    });
  } else {
    err = 'Only for drivers';
    next(err);
  };
}

function getLoadInfo(req, res, next) {
  if (req.user.role === "SHIPPER") {

  }
}

function updateLoad(req, res, next) {
  if (req.user.role === "SHIPPER") {

  }
}

async function changeLoadState(req, res, next) {
  if (req.user.role === "DRIVER") {
    let load = await Load.findOne({ assignedTo: req.user.userId, status: 'ASSIGNED'});
    console.log(load);
    if (load){
      console.log(load.state);
      switch(load.state){
        case "En route to Pick Up": 
          load.state = "Arrived to Pick Up";
          load.save().then(() => res.json({
            message: "Load state changed to 'Arrived to Pick Up'"
          }));
          break;
        case "Arrived to Pick Up": 
          load.state = "En route to delivery";
          load.save().then(() => res.json({
            message: "Load state changed to 'En route to delivery'"
          }));
          break;
        case "En route to delivery":
          load.state = "Arrived to delivery";
          load.status = "SHIPPED";
          await Truck.findOneAndUpdate({ assignedTo: load.assignedTo}, { $set: { status: 'IS' } });
          load.save().then(() => res.json({
            message: "Load state changed to 'Arrived to delivery'"
          }));
          break;
      }
    } else next(err = "No loads");
  } else {
    err = 'Only for drivers';
    next(err);
  };
}

function deleteLoad(req, res, next) {
  if (req.user.role === "SHIPPER") {

  }
}

module.exports = {
  createLoad, createLoadById, getLoads, getLoadById, getActiveLoads, getLoadInfo, updateLoad, changeLoadState,
  deleteLoad,
};
