const { Truck, truckJoiSchema } = require('./models/Trucks.js');

async function createTruck(req, res, next) {
  if (req.user.role === "DRIVER") {
  const { type } = req.body;
  console.log(req.body)
  const truck = new Truck({
      createdBy: req.user.userId,
      type,
      status: 'IS',
      createdDate: new Date(),
    });
    console.log(truck);
    truck.save()
    .then((saved) => res.json({
    message: "Success"
    }))
  } else {
    err = 'Only for drivers';
    next(err);
  };
}



function getTrucks(req, res, next) {
  if (req.user.role === "DRIVER") {
    return Truck.find({ userId: req.user.userId }).then((result) => {
      res.json({'trucks': result});
    });
  } else {
    err = 'Only for drivers';
    next(err);
  };
 }

function getTruckById(req, res, next) {
  if (req.user.role === "DRIVER") {
    return Truck.findOne({ _id: req.params.id, createdBy: req.user.userId })
    .then((truck) => {
        res.json({'truck': truck});
    });
  } else {
    err = 'Only for drivers';
    next(err);
  }
}

async function updateTruck(req, res, next) {
  if (req.user.role == 'DRIVER') {
    const truck = await Truck.findById(req.params.id);
    const { type } = req.body;
    if (type) {
      truck.type = type;
      return truck.save().then((saved) => res.json({massage: 'Successfull', saved}));
    } else next(err = "Nothing to update");
  } else {
    err = 'Only for drivers';
    next(err);
  }
}

async function assignTruck(req, res, next) {
  if (req.user.role === 'DRIVER') {
      Truck.findByIdAndUpdate({ _id: req.params.id}, { $set: { assignedTo: req.user.userId } })
      .then((result) => {
        res.status(200).send({
          message: "Sussess"
        });
      });
    // } else next(err = "No id");
  } else {
    err = 'Only for drivers';
    next(err);
  }
}

function deleteTruck(req, res, next) {
  if (req.user.role === 'DRIVER') {
    return Truck.findByIdAndDelete(req.params.id)
    .then((truck) => {
      res.json({message: 'Success'});
    });
  } else {
    err = 'Only for drivers';
    next(err);
  }
}

module.exports = {
    createTruck, getTrucks, getTruckById, updateTruck, assignTruck, deleteTruck, 
  };