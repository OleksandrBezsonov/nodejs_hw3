const express = require('express');

const router = express.Router();
const {
  createNote, getNotes, getNote, deleteNote, updateNote, getMyNotes, updateMyNoteById,
  markMyNoteCompletedById,
} = require('./notesService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createNote);

// router.get('/', authMiddleware, getNotes);

router.get('/', authMiddleware, getMyNotes);

router.get('/:id', getNote);

// router.patch('/update-my-note/:id', authMiddleware, updateMyNoteById);

router.patch('/:id', authMiddleware, markMyNoteCompletedById);

router.put('/:id', authMiddleware, updateMyNoteById);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
