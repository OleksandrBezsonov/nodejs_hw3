const express = require('express');

const router = express.Router();
const { registerUser, loginUser } = require('./authService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

const asyncWrapper = (controller) => {
  return (req, res, next) => controller(req, res, next).catch(next);
}

router.post('/register', asyncWrapper(registerUser));

router.post('/login', asyncWrapper(loginUser));

module.exports = {
  authRouter: router,
};
