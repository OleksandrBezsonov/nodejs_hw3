const mongoose = require('mongoose');
const Joi = require('joi');

const truckJoiSchema = Joi.object({
  createdBy: Joi.string(),

  assignedTo: Joi.string(),

  type: Joi.string()
    .min(2)
    .max(60),

  status: Joi.string().required().valid('IS','OL'),

  createdDate: Joi.string(),
});

const truckSchema = mongoose.Schema({
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
  },
  assignedTo: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  type: {
    type: String,
  },
  status: {
    type: String,
  },
  createdDate: {
    type: String,
    required: true,
  }
});

const Truck = mongoose.model('Truck', truckSchema);

module.exports = {
  Truck, truckJoiSchema
};