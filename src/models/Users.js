const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  email: Joi.string()
    .required(),

  password: Joi.string()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

  role: Joi.string().valid('DRIVER','SHIPPER'),

  createdDate: Joi.string(),
});

const User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
});

module.exports = {
  User,
  userJoiSchema,
};
