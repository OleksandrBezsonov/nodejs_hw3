const mongoose = require('mongoose');
const Joi = require('joi');

const loadJoiSchema = Joi.object({
  createdBy: Joi.string().required(),

  assignedTo: Joi.string().required(),

  status: Joi.string().valid('NEW','POSTED', 'ASSIGNED', 'SHIPPED'),
 
  state: Joi.string()
  .min(2)
  .max(60),

  name: Joi.string().required()
  .min(2)
  .max(60),

  payload: Joi.string()
  .alphanum()
  .min(2)
  .max(60),

  pickup_address: Joi.string()
  .min(2)
  .max(60),

  delivery_address: Joi.string()
  .min(2)
  .max(60),

  dimensions: Joi.object().required().keys({
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  }),

  createdDate: Joi.string()
  .min(2)
  .max(60)
});

const loadSchema = mongoose.Schema({
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assignedTo: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
    default: null,
  },
  name: {
    type: String,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
      width: String,
      length: String,
      height: String,
  },
  logs: {
    type: [String],
  },
  createdDate: {
    type: String,
    required: true,
  }
});

const Load = mongoose.model('Load', loadSchema);

module.exports = {
  Load, loadJoiSchema
};