const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User, userJoiSchema } = require('./models/Users.js');

const registerUser = async (req, res, next) => {
  const { email, password, role } = req.body;
  await userJoiSchema.validateAsync({email, password, role});
  try {
    const user = new User({
      email,
      password: await bcrypt.hash(password, 10),
      role,
      createdDate: new Date(),
    });
    user.save()
      .then((saved) => res.json({
        message: "Success"
      }))
      .catch((err) => {
        next(err);
      });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const loginUser = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
      const payload = { 
        email: user.email, role: user.role, userId: user._id };
      const jwtToken = jwt.sign(payload, 'secret-jwt-key');
      return res.json({ 
        message: "Success",
        jwt_token: jwtToken 
      });
    }
    return res.status(400).json({ message: 'Not authorized' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

module.exports = {
  registerUser,
  loginUser,
};
